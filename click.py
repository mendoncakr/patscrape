import pdb
import time
import threading

from selenium import webdriver

options = webdriver.ChromeOptions()
options.add_argument('headless')

prefs = {"profile.managed_default_content_settings.images": 2}
options.add_experimental_option("prefs", prefs)

MAX_RETRIES = 2
FACTOR = 5
VOTES_TO_ADD = 10
URL = "http://www.competitionplus.com/content/lions-e2-kenny-delco-vs-vincent-nobile"


def vote_kenny(retry_string=None, total_retries=0):
    if retry_string:
        print(retry_string)

    if total_retries > 0:
        print("Total Retries: {}".format(total_retries))

    if total_retries >= MAX_RETRIES:
        return

    try:
        driver = webdriver.Chrome('./chromedriver', chrome_options=options)
        driver.get(URL)

        vote_button = driver.find_element_by_name('op')
        if vote_button:
            time.sleep(0.3) # wait for page to load
            radio_buttons = driver.find_element_by_class_name('form-radios')
            choices = radio_buttons.find_elements_by_tag_name('label')
            kenny = [choice for choice in choices if 'kenny' in choice.text.lower()][0].click()
            vote_form = driver.find_element_by_class_name('vote-form').submit()
            print("Voting for Kenny...")

        return driver.quit()
    except Exception as err:
        total_retries += 1
        vote_kenny(retry_string="Retrying.....", total_retries=total_retries)



if __name__ == "__main__":
    for i in range(1, FACTOR + 1):
        print("Round: {}".format(i))
        threads = []
        for j in range(1, VOTES_TO_ADD + 1):
            print("Thread: {}".format(j))

            thread = threading.Thread(target=vote_kenny, args=())
            thread.start()
            threads.append(thread)

        # wait for threads
        waiting = [thread.join() for thread in threads]


# https://seleniumjava.com/2015/12/12/how-to-make-selenium-webdriver-scripts-faster/